from flask import Flask, request
from esg.calculator import calculate_carbon
# Flask constructor takes the name of
# current module (__name__) as argument.
app = Flask(__name__)
 
# The route() function of the Flask class is a decorator,
# which tells the application which URL should call
# the associated function.
@app.route('/calculate', methods = ['POST'])
def calculate():
    emission =  calculate_carbon(request.json)
    print(emission)
    return {"total_yearly_CO_emission": emission}
 
# main driver function
if __name__ == '__main__':
 
    # run() method of Flask class runs the application
    # on the local development server.
    app.run()