#Calculations for the carbon footprint calculator on this were gathered from equations and 
#formulas provided publicly by the Environmental Protection Agency and the University of California,
#Berkeley's Institute of the Environment.

def electricity(average_total_bill, price_per_kwh):
    factor = 1.37
    emission = (average_total_bill / price_per_kwh) * factor * 12
    return emission


def natural_gas(average_total_bill, price_thousand_cubic_feet):
    factor = 120.61
    emission = (average_total_bill / price_thousand_cubic_feet) * factor * 12
    return emission

def fuel_oil(average_total_bill, price_per_gallon):
    factor = 22.37
    emission = (average_total_bill / price_per_gallon) * factor * 12
    return emission

def propane(average_total_bill, price_per_gallon):
    factor = 12.17
    emission = (average_total_bill / price_per_gallon) * factor * 12
    return emission

def vechile(average_miles_per_week, fuel_miles_per_gallon):
    factor = 19.4
    emissions_of_greenhouse_gases = 100 / 95
    emission = (average_miles_per_week * 52.1 / fuel_miles_per_gallon) * factor * emissions_of_greenhouse_gases
    return emission

def flight(average_miles_per_year, ):
    emission = average_miles_per_year * 1.6 * 1.09

def food():
    pass

def calculate_carbon(body):
    running_emission = 0
    
    running_emission += electricity(body["electricity_average_monthly_bill"], body["electricity_price_per_kwh"])
    running_emission += natural_gas(body["natural_gas_average_monthly_bill"], body["natural_gas_price_per_thousand_cubic_feet"])
    running_emission += fuel_oil(body["fuel_oil_average_monthly_bill"], body["fuel_oil_price_per_gallon"])
    running_emission += propane(body["propane_average_monthly_bill"], body["propane_price_per_gallon"])
    running_emission += vechile(body["vechile_average_miles_per_week"], body["vechile_miles_per_gallon"])
    
    return running_emission